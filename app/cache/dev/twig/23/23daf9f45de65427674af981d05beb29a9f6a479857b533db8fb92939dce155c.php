<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_7e91646406e3cdb355bce0d1be1bcb920f4b50aa28494f046476e8809e544184 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c96e68c007bb2d2308784ba64acd5b18e05221e120f29b11ef2719104864158d = $this->env->getExtension("native_profiler");
        $__internal_c96e68c007bb2d2308784ba64acd5b18e05221e120f29b11ef2719104864158d->enter($__internal_c96e68c007bb2d2308784ba64acd5b18e05221e120f29b11ef2719104864158d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c96e68c007bb2d2308784ba64acd5b18e05221e120f29b11ef2719104864158d->leave($__internal_c96e68c007bb2d2308784ba64acd5b18e05221e120f29b11ef2719104864158d_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_eacf3be57cd3d6e409f79001f1caa37e844df7c3c2eb4b005c64dbe33ee7566e = $this->env->getExtension("native_profiler");
        $__internal_eacf3be57cd3d6e409f79001f1caa37e844df7c3c2eb4b005c64dbe33ee7566e->enter($__internal_eacf3be57cd3d6e409f79001f1caa37e844df7c3c2eb4b005c64dbe33ee7566e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_eacf3be57cd3d6e409f79001f1caa37e844df7c3c2eb4b005c64dbe33ee7566e->leave($__internal_eacf3be57cd3d6e409f79001f1caa37e844df7c3c2eb4b005c64dbe33ee7566e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_0fcbf8c1e241d856de3c33031d4edcf794bf92c407c1eb10d2119692bbe8c786 = $this->env->getExtension("native_profiler");
        $__internal_0fcbf8c1e241d856de3c33031d4edcf794bf92c407c1eb10d2119692bbe8c786->enter($__internal_0fcbf8c1e241d856de3c33031d4edcf794bf92c407c1eb10d2119692bbe8c786_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_0fcbf8c1e241d856de3c33031d4edcf794bf92c407c1eb10d2119692bbe8c786->leave($__internal_0fcbf8c1e241d856de3c33031d4edcf794bf92c407c1eb10d2119692bbe8c786_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
