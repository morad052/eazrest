<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_fa529eab600d384f057241f9886915da50ffcbd0dba2317ecab8e58ba99f3860 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5732da5d84041e51c5c525ae9eaad54907383c62469a358a1fb1d71da01bbb8b = $this->env->getExtension("native_profiler");
        $__internal_5732da5d84041e51c5c525ae9eaad54907383c62469a358a1fb1d71da01bbb8b->enter($__internal_5732da5d84041e51c5c525ae9eaad54907383c62469a358a1fb1d71da01bbb8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_5732da5d84041e51c5c525ae9eaad54907383c62469a358a1fb1d71da01bbb8b->leave($__internal_5732da5d84041e51c5c525ae9eaad54907383c62469a358a1fb1d71da01bbb8b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
