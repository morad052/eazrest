<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_54d41d8a0fefe8a3f5a1aedf0cca41eca6248e32462fe76f326e4f9a3864a549 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_157b940d06401674d724d637dafd727a2a8c4e8de9ca8d54f311e0b40b789146 = $this->env->getExtension("native_profiler");
        $__internal_157b940d06401674d724d637dafd727a2a8c4e8de9ca8d54f311e0b40b789146->enter($__internal_157b940d06401674d724d637dafd727a2a8c4e8de9ca8d54f311e0b40b789146_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("TwigBundle:Exception:exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_157b940d06401674d724d637dafd727a2a8c4e8de9ca8d54f311e0b40b789146->leave($__internal_157b940d06401674d724d637dafd727a2a8c4e8de9ca8d54f311e0b40b789146_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include 'TwigBundle:Exception:exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
