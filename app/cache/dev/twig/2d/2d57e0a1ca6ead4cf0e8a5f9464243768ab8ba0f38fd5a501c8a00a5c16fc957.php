<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_6e22386dd9bdd48163f57d77737265bdbbf67f8a7a509f4e694d71e5f1f1ac23 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cd62dbf175f1b8e4877fa97ac2ddc718e738539b0617a9c300949b59c2622919 = $this->env->getExtension("native_profiler");
        $__internal_cd62dbf175f1b8e4877fa97ac2ddc718e738539b0617a9c300949b59c2622919->enter($__internal_cd62dbf175f1b8e4877fa97ac2ddc718e738539b0617a9c300949b59c2622919_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("TwigBundle:Exception:exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_cd62dbf175f1b8e4877fa97ac2ddc718e738539b0617a9c300949b59c2622919->leave($__internal_cd62dbf175f1b8e4877fa97ac2ddc718e738539b0617a9c300949b59c2622919_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include 'TwigBundle:Exception:exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
