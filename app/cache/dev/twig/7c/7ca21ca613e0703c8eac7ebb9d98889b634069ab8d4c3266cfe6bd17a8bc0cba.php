<?php

/* EazTagapiBundle:Default:index.html.twig */
class __TwigTemplate_90bae1d66cdf47f77dc31543109dc8d0a2860d3c8f5f896a5200f20ce01ffde4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0467f8c9e60886b2ac67529fe9090519c80ac16f2c9760dd9ceedd195e0faf2a = $this->env->getExtension("native_profiler");
        $__internal_0467f8c9e60886b2ac67529fe9090519c80ac16f2c9760dd9ceedd195e0faf2a->enter($__internal_0467f8c9e60886b2ac67529fe9090519c80ac16f2c9760dd9ceedd195e0faf2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EazTagapiBundle:Default:index.html.twig"));

        // line 1
        echo "Hello ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "!
";
        
        $__internal_0467f8c9e60886b2ac67529fe9090519c80ac16f2c9760dd9ceedd195e0faf2a->leave($__internal_0467f8c9e60886b2ac67529fe9090519c80ac16f2c9760dd9ceedd195e0faf2a_prof);

    }

    public function getTemplateName()
    {
        return "EazTagapiBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* Hello {{ name }}!*/
/* */
