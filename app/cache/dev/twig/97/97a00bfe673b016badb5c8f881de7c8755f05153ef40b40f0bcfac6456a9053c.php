<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_c75da75395ac73faaacd982d79220808758e472e75fe2dacb09b8dbe8c6082a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b519615a598e850029ab1ad218d7dc0ec85b6e9f9f20116a5e89241ac9ec24ad = $this->env->getExtension("native_profiler");
        $__internal_b519615a598e850029ab1ad218d7dc0ec85b6e9f9f20116a5e89241ac9ec24ad->enter($__internal_b519615a598e850029ab1ad218d7dc0ec85b6e9f9f20116a5e89241ac9ec24ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_b519615a598e850029ab1ad218d7dc0ec85b6e9f9f20116a5e89241ac9ec24ad->leave($__internal_b519615a598e850029ab1ad218d7dc0ec85b6e9f9f20116a5e89241ac9ec24ad_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_5b12d8d62e7ed22c8b808a398fbda15c83d5ca2492f7409cc49c0bcd9e24f7e3 = $this->env->getExtension("native_profiler");
        $__internal_5b12d8d62e7ed22c8b808a398fbda15c83d5ca2492f7409cc49c0bcd9e24f7e3->enter($__internal_5b12d8d62e7ed22c8b808a398fbda15c83d5ca2492f7409cc49c0bcd9e24f7e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_5b12d8d62e7ed22c8b808a398fbda15c83d5ca2492f7409cc49c0bcd9e24f7e3->leave($__internal_5b12d8d62e7ed22c8b808a398fbda15c83d5ca2492f7409cc49c0bcd9e24f7e3_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
