<?php

/* SensioDistributionBundle:Configurator:layout.html.twig */
class __TwigTemplate_4fa88ad0cd6018a55f748f108496b7da2a0375ace9ff154d5832675c2b9de9dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "SensioDistributionBundle:Configurator:layout.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0d1d58123e0c52cac173d6a74a02aac489484db7507327be9d5957fd9d382ed7 = $this->env->getExtension("native_profiler");
        $__internal_0d1d58123e0c52cac173d6a74a02aac489484db7507327be9d5957fd9d382ed7->enter($__internal_0d1d58123e0c52cac173d6a74a02aac489484db7507327be9d5957fd9d382ed7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SensioDistributionBundle:Configurator:layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0d1d58123e0c52cac173d6a74a02aac489484db7507327be9d5957fd9d382ed7->leave($__internal_0d1d58123e0c52cac173d6a74a02aac489484db7507327be9d5957fd9d382ed7_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_4a84b5e83c50c320b21197a8fea249ee9afd9731017fc404748df4db3f096d09 = $this->env->getExtension("native_profiler");
        $__internal_4a84b5e83c50c320b21197a8fea249ee9afd9731017fc404748df4db3f096d09->enter($__internal_4a84b5e83c50c320b21197a8fea249ee9afd9731017fc404748df4db3f096d09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sensiodistribution/webconfigurator/css/configurator.css"), "html", null, true);
        echo "\" />
";
        
        $__internal_4a84b5e83c50c320b21197a8fea249ee9afd9731017fc404748df4db3f096d09->leave($__internal_4a84b5e83c50c320b21197a8fea249ee9afd9731017fc404748df4db3f096d09_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_533f46e53541d33d9e214ea10d56993c48cf73ea77445126a6714c34302b5379 = $this->env->getExtension("native_profiler");
        $__internal_533f46e53541d33d9e214ea10d56993c48cf73ea77445126a6714c34302b5379->enter($__internal_533f46e53541d33d9e214ea10d56993c48cf73ea77445126a6714c34302b5379_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Web Configurator Bundle";
        
        $__internal_533f46e53541d33d9e214ea10d56993c48cf73ea77445126a6714c34302b5379->leave($__internal_533f46e53541d33d9e214ea10d56993c48cf73ea77445126a6714c34302b5379_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_d2a1006d2ca30a9abaf27fe752619e4d8d8642908d7b7e2103987b6c24cfa268 = $this->env->getExtension("native_profiler");
        $__internal_d2a1006d2ca30a9abaf27fe752619e4d8d8642908d7b7e2103987b6c24cfa268->enter($__internal_d2a1006d2ca30a9abaf27fe752619e4d8d8642908d7b7e2103987b6c24cfa268_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "    <div class=\"block\">
        ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 12
        echo "    </div>
    <div class=\"version\">Symfony Standard Edition v.";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["version"]) ? $context["version"] : $this->getContext($context, "version")), "html", null, true);
        echo "</div>
";
        
        $__internal_d2a1006d2ca30a9abaf27fe752619e4d8d8642908d7b7e2103987b6c24cfa268->leave($__internal_d2a1006d2ca30a9abaf27fe752619e4d8d8642908d7b7e2103987b6c24cfa268_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_90e1132235c5c31a242888f0e0df0be52e7297da5a1791e7231be6cf876ac608 = $this->env->getExtension("native_profiler");
        $__internal_90e1132235c5c31a242888f0e0df0be52e7297da5a1791e7231be6cf876ac608->enter($__internal_90e1132235c5c31a242888f0e0df0be52e7297da5a1791e7231be6cf876ac608_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_90e1132235c5c31a242888f0e0df0be52e7297da5a1791e7231be6cf876ac608->leave($__internal_90e1132235c5c31a242888f0e0df0be52e7297da5a1791e7231be6cf876ac608_prof);

    }

    public function getTemplateName()
    {
        return "SensioDistributionBundle:Configurator:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 11,  79 => 13,  76 => 12,  74 => 11,  71 => 10,  65 => 9,  53 => 7,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends "TwigBundle::layout.html.twig" %}*/
/* */
/* {% block head %}*/
/*     <link rel="stylesheet" href="{{ asset('bundles/sensiodistribution/webconfigurator/css/configurator.css') }}" />*/
/* {% endblock %}*/
/* */
/* {% block title 'Web Configurator Bundle' %}*/
/* */
/* {% block body %}*/
/*     <div class="block">*/
/*         {% block content %}{% endblock %}*/
/*     </div>*/
/*     <div class="version">Symfony Standard Edition v.{{ version }}</div>*/
/* {% endblock %}*/
/* */
