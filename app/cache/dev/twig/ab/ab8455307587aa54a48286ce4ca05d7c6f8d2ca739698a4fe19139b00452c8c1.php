<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_ae74036aed2413242f0e8d3dd7899cb3c2823e7a6cb2c4d6a70f1d281a4f664b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5537bc7fa5fc996c522e96c22b338bb981f859e674c69e6698aeec386f084bf0 = $this->env->getExtension("native_profiler");
        $__internal_5537bc7fa5fc996c522e96c22b338bb981f859e674c69e6698aeec386f084bf0->enter($__internal_5537bc7fa5fc996c522e96c22b338bb981f859e674c69e6698aeec386f084bf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("TwigBundle:Exception:exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_5537bc7fa5fc996c522e96c22b338bb981f859e674c69e6698aeec386f084bf0->leave($__internal_5537bc7fa5fc996c522e96c22b338bb981f859e674c69e6698aeec386f084bf0_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include 'TwigBundle:Exception:exception.xml.twig' with { 'exception': exception } %}*/
/* */
