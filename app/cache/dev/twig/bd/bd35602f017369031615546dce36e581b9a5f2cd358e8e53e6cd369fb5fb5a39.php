<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_80cbca3cdbb11a173c81303b02df89ba5bb3eb6ba36712b28853b9fd5dc0e3cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_482d4bd611a705ebd09350be716aad3e7a196ba05818a4af2639ac3779c36cc0 = $this->env->getExtension("native_profiler");
        $__internal_482d4bd611a705ebd09350be716aad3e7a196ba05818a4af2639ac3779c36cc0->enter($__internal_482d4bd611a705ebd09350be716aad3e7a196ba05818a4af2639ac3779c36cc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("TwigBundle:Exception:exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_482d4bd611a705ebd09350be716aad3e7a196ba05818a4af2639ac3779c36cc0->leave($__internal_482d4bd611a705ebd09350be716aad3e7a196ba05818a4af2639ac3779c36cc0_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include 'TwigBundle:Exception:exception.xml.twig' with { 'exception': exception } %}*/
/* */
