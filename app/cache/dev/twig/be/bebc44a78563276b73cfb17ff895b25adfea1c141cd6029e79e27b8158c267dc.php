<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_2f831d66a5bb9a1950d3c369ba7a3c7fcd3e706d5f7fd75c5125257ed730ae03 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_02961904bc44ea87e89235c28c90cbbc6395592331eb7d45a4777acad995478d = $this->env->getExtension("native_profiler");
        $__internal_02961904bc44ea87e89235c28c90cbbc6395592331eb7d45a4777acad995478d->enter($__internal_02961904bc44ea87e89235c28c90cbbc6395592331eb7d45a4777acad995478d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_02961904bc44ea87e89235c28c90cbbc6395592331eb7d45a4777acad995478d->leave($__internal_02961904bc44ea87e89235c28c90cbbc6395592331eb7d45a4777acad995478d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
