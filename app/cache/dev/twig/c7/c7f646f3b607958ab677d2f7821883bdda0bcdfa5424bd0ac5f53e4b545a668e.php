<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_23022db1b4f8bf4f5cbf6b551925af97d9f785d3e3d6aef863b94f416d096b83 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_919f3a48a3d91ca72e5d6156b2585157d78bb01b16c19521aa99902b459ae8c2 = $this->env->getExtension("native_profiler");
        $__internal_919f3a48a3d91ca72e5d6156b2585157d78bb01b16c19521aa99902b459ae8c2->enter($__internal_919f3a48a3d91ca72e5d6156b2585157d78bb01b16c19521aa99902b459ae8c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_919f3a48a3d91ca72e5d6156b2585157d78bb01b16c19521aa99902b459ae8c2->leave($__internal_919f3a48a3d91ca72e5d6156b2585157d78bb01b16c19521aa99902b459ae8c2_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
