<?php

/* ::base.html.twig */
class __TwigTemplate_7062fcd29863d9f689b6a9a1b56b459377404ffb2dc6956dd92de8c533a9fe31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_34135048bf814b8e7f0c17664d85ce313ab3192ed12b13f30006afa2d8292932 = $this->env->getExtension("native_profiler");
        $__internal_34135048bf814b8e7f0c17664d85ce313ab3192ed12b13f30006afa2d8292932->enter($__internal_34135048bf814b8e7f0c17664d85ce313ab3192ed12b13f30006afa2d8292932_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_34135048bf814b8e7f0c17664d85ce313ab3192ed12b13f30006afa2d8292932->leave($__internal_34135048bf814b8e7f0c17664d85ce313ab3192ed12b13f30006afa2d8292932_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_a2974b5ac13964460a9dafbd4be6bd9399f80f8f03c3ee4fd724913539390224 = $this->env->getExtension("native_profiler");
        $__internal_a2974b5ac13964460a9dafbd4be6bd9399f80f8f03c3ee4fd724913539390224->enter($__internal_a2974b5ac13964460a9dafbd4be6bd9399f80f8f03c3ee4fd724913539390224_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_a2974b5ac13964460a9dafbd4be6bd9399f80f8f03c3ee4fd724913539390224->leave($__internal_a2974b5ac13964460a9dafbd4be6bd9399f80f8f03c3ee4fd724913539390224_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b43c03d00a8e1e20d50e61217f7754c96b77371a66bd4c777e736cf86e977b63 = $this->env->getExtension("native_profiler");
        $__internal_b43c03d00a8e1e20d50e61217f7754c96b77371a66bd4c777e736cf86e977b63->enter($__internal_b43c03d00a8e1e20d50e61217f7754c96b77371a66bd4c777e736cf86e977b63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_b43c03d00a8e1e20d50e61217f7754c96b77371a66bd4c777e736cf86e977b63->leave($__internal_b43c03d00a8e1e20d50e61217f7754c96b77371a66bd4c777e736cf86e977b63_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_4d64da9f16f636ab9efba0fa6228325c005db219bf4732eeccfd49456e0c1d99 = $this->env->getExtension("native_profiler");
        $__internal_4d64da9f16f636ab9efba0fa6228325c005db219bf4732eeccfd49456e0c1d99->enter($__internal_4d64da9f16f636ab9efba0fa6228325c005db219bf4732eeccfd49456e0c1d99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_4d64da9f16f636ab9efba0fa6228325c005db219bf4732eeccfd49456e0c1d99->leave($__internal_4d64da9f16f636ab9efba0fa6228325c005db219bf4732eeccfd49456e0c1d99_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_62910c829dc5fbb433ba8018844412c170aecf56fb3ab35ac331688ec8406d37 = $this->env->getExtension("native_profiler");
        $__internal_62910c829dc5fbb433ba8018844412c170aecf56fb3ab35ac331688ec8406d37->enter($__internal_62910c829dc5fbb433ba8018844412c170aecf56fb3ab35ac331688ec8406d37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_62910c829dc5fbb433ba8018844412c170aecf56fb3ab35ac331688ec8406d37->leave($__internal_62910c829dc5fbb433ba8018844412c170aecf56fb3ab35ac331688ec8406d37_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
