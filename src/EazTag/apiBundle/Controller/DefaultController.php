<?php

namespace EazTag\apiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('EazTagapiBundle:Default:index.html.twig', array('name' => $name));
    }
}
