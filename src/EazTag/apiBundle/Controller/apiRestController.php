<?php

namespace EazTag\apiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use EazTag\apiBundle\Entity\Pays;

class apiRestController extends Controller
{
    public function getPaysAction()
    {
        $pays = $this->getDoctrine()->getRepository('EazTagapiBundle:Pays')->findAll();
        return $pays;
    }
    
        public function getVilleAction($pays)
    {
        $ville = $this->getDoctrine()->getRepository('EazTagapiBundle:Ville')->findByVille($pays);
        return $ville;
    }
}
